# About baby-keyboard

This program lets you set up a computer keyboard to play a sound (optionally a different sound per key) when any key is pressed.

It uses Linux-specific input device logic to read from the keyboard, so requires that the keyboard is connected to a Linux system.

It is intended to help babies / young children learn about cause and effect (their button press causes a sound to play), and get them interested in computer hardware even before they are ready to use screens.

# Requirements

* A Linux system
* An extra keyboard (other than the one you normally type on), preferably a different model of keyboard to make identification easier.

# Setting up your keyboard

Assuming you are also going to use the computer for other computing tasks, you won't want any keypresses by the baby to register in your other programs.

If you are using X11, you achieve this by "floating" the keyboard input, i.e. telling X11 not to read events from it.

The first step is to find the correct USB device. Try running `ls /dev/input/by-id/*kbd*` with and without the extra keyboard plugged in and identify the extra entry. In my case, it is `/dev/input/by-id/usb-0b38_USB-compliant_keyboard-event-kbd`. Drop the `/dev/input/by-id/` from the start and the `-event-kbd` from the end to get the ID - in my case, just `usb-0b38_USB-compliant_keyboard`.

The `setup-device.sh` script at the top-level contains a line like this: `TARGET_SERIAL="0b38_USB-compliant_keyboard"`. Change it to match the ID you obtained from the above step. Once you have done this, run it with `./setup-device.sh`.

The script will print out the IDs of all the devices that matched, and will float them so they no longer function as a normal keyboard for input (until you restart X11 - for example, by logging out, or restarting your computer, or until you disconnect and reconnect the keyboard, after which you will need to run the script again). The most important part is the first part of the line - e.g. `Matched /dev/input/event11`. Note down that device name (e.g. `/dev/input/event11`) as you will need it later. If you have more than one different device name come up, either you have several identical keyboards connected, or your keyboard is actually several devices (some keyboards have one device for normal keys, and one for extra keys). In the latter case, you will have to experiment to find the correct ID.

# Building and running

Install Haskell Stack by following the instructions at https://docs.haskellstack.org/en/stable/install_and_upgrade/

Run `stack build && stack exec baby-keyboard-exe /dev/input/event11` - replace `/dev/input/event11` with the correct device name obtained from when you ran `./setup-device.sh`.

# Picking sounds

The program doesn't come with any sounds, so you need to set up sounds to play. It comes with a script to easily download sounds from [http://soundbible.com/]. Find a sound that you want, and note down the number from the URL - e.g. if you pick http://soundbible.com/1296-Horse-Neigh.html, the number is 1296. You also need to give the sound a descriptive name to make the next step easier - this can be anything you like, e.g. horseneigh. Change into the `sounds/raw` directory, and run `./fetch-from-soundbible.sh 1296 horseneigh`. This will create a file called `horseneigh.raw` containing the sound sample.

To set the sound as the default (for any key without another sound mapped), run a command like `ln -s ../raw/horseneigh.raw ../map/default`.

To set the sound for a key (to override the default), you need to find the numeric keycode for the key - it is printed when you push the key. Suppose you wanted to map left shift (42) to horseneigh, and you are already in the `sounds/raw` directory. Run `ln -s ../raw/horseneigh.raw ../map/42`