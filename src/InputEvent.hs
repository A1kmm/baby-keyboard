module InputEvent (InputEvent(..), parseInputEvents) where

import ForeignEvent
import qualified Data.ByteString as BS
import qualified Data.ByteString.Unsafe as BS
import Foreign.Ptr
import System.IO.Unsafe (unsafePerformIO)

data InputEvent = InputEvent {
  eventTiming :: Double,
  eventEvent :: Int,
  eventCode :: Int,
  eventValue :: Int
                             } deriving (Eq, Ord, Show)

parseOneInputEvent :: BS.ByteString -> InputEvent
parseOneInputEvent bs = unsafePerformIO $ BS.unsafeUseAsCString bs $ \ptr ->
  do
    let inputEvent = castPtr ptr
    time <- rawGetInputTime inputEvent
    timeUsec <- rawGetInputTimeUsec inputEvent
    event <- rawGetInputEvent inputEvent
    code <- rawGetInputCode inputEvent
    value <- rawGetInputValue inputEvent
    return $ InputEvent {
      eventTiming = (fromIntegral time) + (fromIntegral timeUsec) / 1000.0,
      eventEvent = fromIntegral event,
      eventCode = fromIntegral code,
      eventValue = fromIntegral value
    }
  

parseInputEvents :: BS.ByteString -> [InputEvent]
parseInputEvents bs =
  if BS.length bs < inputEventSize then
    []
  else
    let
      (thisEvent, rest) = BS.splitAt inputEventSize bs
    in
      (parseOneInputEvent thisEvent):(parseInputEvents rest)

