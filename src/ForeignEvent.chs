module ForeignEvent where
import Foreign.C.Types
#include <input_types.h>

type InputEventPtr = {#type input_event#}
type EventTimePtr = {#type event_time#}

inputEventSize :: Int
inputEventSize = {#sizeof input_event#}

rawGetInputTime :: InputEventPtr -> IO CLong
rawGetInputTime = {#get input_event->time.tv_sec#}

rawGetInputTimeUsec :: InputEventPtr -> IO CLong
rawGetInputTimeUsec = {#get input_event->time.tv_usec#}

rawGetInputEvent :: InputEventPtr -> IO CUShort
rawGetInputEvent = {#get input_event->type#}

rawGetInputCode :: InputEventPtr -> IO CUShort
rawGetInputCode = {#get input_event->code#}

rawGetInputValue :: InputEventPtr -> IO CInt
rawGetInputValue = {#get input_event->value#}
