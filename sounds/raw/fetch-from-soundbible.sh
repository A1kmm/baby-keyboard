#!/bin/bash

ID=$1
NAME=$2

if [[ -z $NAME ]]; then
    echo "Usage: $0 idnumber soundname"
fi

curl -v -v -L -o "$NAME.wav" "http://soundbible.com/grab.php?id=$ID&type=wav" -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-GB,en;q=0.5' --compressed -H 'Referer: http://soundbible.com/1510-Rooster.html' -H 'Cookie: __unam=7639673-16628592c4f-1b245c98-18'
sox "$NAME.wav" -b 16 -t raw -r 44100 -c 1 --endian little "$NAME.raw"
