#!/bin/bash

MAP="$(dirname $0)/../sounds/map"
(for MAPPING in $MAP/*; do
    MAPPING=$(basename $MAPPING)
    MAPSTO=$(basename $(readlink $MAP/$MAPPING) .raw)
    echo "$MAPPING,$MAPSTO"
done) >/tmp/sounds.csv

join -t "," -1 1 -2 2 /tmp/sounds.csv "$(dirname $0)/keys.csv" | cut -d"," -f2-
