#!/bin/bash

TARGET_SERIAL="0b38_USB-compliant_keyboard"

XDEVICES=$(xinput --list --id-only | tr -d "∼ ")

for XDEV in $XDEVICES; do
    INPUTDEV=$(xinput list-props $XDEV | grep "Device Node (265):" | cut -c22- | tr -d "\"")
    if [[ -n $INPUTDEV ]]; then
        DEVSERIAL=$(udevadm info --query=property --name=$INPUTDEV | grep ID_SERIAL)
        if [[ $DEVSERIAL == "ID_SERIAL=$TARGET_SERIAL" ]]; then
            echo Matched $INPUTDEV on $DEVSERIAL / $XDEV;
            xinput --float $XDEV
        fi
    fi
done


