{-# LANGUAGE TypeApplications #-}
module Main where
import System.IO
import qualified Data.ByteString as BS
import qualified Data.ByteString.Unsafe as BS
import qualified Data.ByteString.Char8 as CBS
import Foreign.Ptr
import Sound.ALSA.PCM
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM
import Control.Concurrent
import Control.Exception
import System.Environment
import InputEvent
import Data.Int

type PlayQueue = TVar BS.ByteString

isKeyDown :: InputEvent -> Bool
isKeyDown ev = eventEvent ev == 1 && eventValue ev == 1

loadFileFor :: String -> IO BS.ByteString
loadFileFor codeString =
  catch @IOException (BS.readFile ("sounds/map/" ++ codeString)) $ \_ ->
    BS.readFile "sounds/map/default"

replaceQueueWith :: BS.ByteString -> PlayQueue -> IO ()
replaceQueueWith snd pq = atomically $
  writeTVar pq snd

handleKeyDown :: PlayQueue -> InputEvent -> IO ()
handleKeyDown pq ev = do
  let eventStr = (show . eventCode $ ev)
  putStrLn $ "Got key " ++ eventStr
  sound <- loadFileFor eventStr
  replaceQueueWith sound pq

keyboardLoop :: Handle -> PlayQueue -> IO ()
keyboardLoop hdl pq = do
  event <- BS.hGetSome hdl 1000
  mapM_ (handleKeyDown pq) $ filter isKeyDown $ parseInputEvents event
  keyboardLoop hdl pq

sampleRate = 44100
soundFormat :: SoundFmt Int16
soundFormat = SoundFmt sampleRate

soundSink :: SoundSink Pcm Int16
soundSink = alsaSoundSink "default" soundFormat

audioLoop :: Pcm Int16 -> PlayQueue -> IO ()
audioLoop sink pq = do
  sample <- atomically $ do
    currentBuffer <- readTVar pq
    check . not . BS.null $ currentBuffer
    let (takeBuffer, keepBuffer) = BS.splitAt 1024 currentBuffer
    writeTVar pq keepBuffer
    return takeBuffer
  let sampleLength = BS.length sample `div` 2
  BS.unsafeUseAsCString sample $ \ptr ->
    soundSinkWrite soundSink sink (castPtr ptr) sampleLength
  audioLoop sink pq

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> putStrLn "Usage: baby-keyboard inputdevice"
    (inputDev:_) -> do
      inputHandle <- openFile inputDev ReadMode
      playQueue <- newTVarIO BS.empty
      forkIO $ keyboardLoop inputHandle playQueue
      
      sink <- soundSinkOpen soundSink
      soundSinkStart soundSink sink
      audioLoop sink playQueue
